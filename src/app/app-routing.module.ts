import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [

  {
    path: '',
    redirectTo: 'stage1',
    pathMatch: 'full'
  },
  {
    path: 'stage2',
    loadChildren: () => import('./pages/stage2/stage2.module').then( m => m.Stage2PageModule)
  },
  {
    path: 'stage1',
    loadChildren: () => import('./pages/stage1/stage1.module').then( m => m.Stage1PageModule)
  },
  {
    path: 'stage3',
    loadChildren: () => import('./pages/stage3/stage3.module').then( m => m.Stage3PageModule)
  },
  {
    path: 'stage4',
    loadChildren: () => import('./pages/stage4/stage4.module').then( m => m.Stage4PageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
