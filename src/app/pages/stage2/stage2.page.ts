import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators, FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-stage2',
  templateUrl: './stage2.page.html',
  styleUrls: ['./stage2.page.scss'],
})
export class Stage2Page implements OnInit {

  envioForm:FormGroup;
  carritoForm: FormGroup;
  forms = [];
  sub: any;
  constructor(private router:Router, private route: ActivatedRoute, public fb:FormBuilder) {
    this.sub = this.route.params.subscribe(params => {
      this.carritoForm = params['carritoForm'];
    });
    this.envioForm = this.fb.group({envio:['', Validators.required]});
    this.forms = [this.carritoForm,this.envioForm]
   }

  ngOnInit() {
  }

  siguientePagina(){
    console.log(this.forms);
    this.router.navigate(['/stage3', this.forms])
  }

  volver(){
    this.router.navigate(['/stage1'])
  }

}
