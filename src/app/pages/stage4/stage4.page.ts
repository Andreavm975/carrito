import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-stage4',
  templateUrl: './stage4.page.html',
  styleUrls: ['./stage4.page.scss'],
})
export class Stage4Page implements OnInit {

  forms = [];
  sub: any;

  constructor(private router:Router, private route: ActivatedRoute) { 
    this.sub = this.route.params.subscribe(params => {
      this.forms = params['forms'];
    });
  }

  ngOnInit() {
  }

  volverInicio(){
    this.router.navigate(['/stage1'])
  }

}
