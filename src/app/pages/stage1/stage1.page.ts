import { Component, OnInit } from '@angular/core';

import { NavigationExtras, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validator, Validators } from '@angular/forms';

@Component({
  selector: 'app-stage1',
  templateUrl: './stage1.page.html',
  styleUrls: ['./stage1.page.scss'],
})
export class Stage1Page implements OnInit {
  carritoForm: FormGroup;
  

  constructor(private router: Router,public fb:FormBuilder) {
    this.carritoForm = this.fb.group({
      promo:'',
      email:['', Validators.required],
      consentimiento: [false, Validators.required],
      nombre:['', Validators.required],
      apellido:['', Validators.required],
      empresa:'',
      direccion:['', Validators.required],
      direccion2:'',
      pais:['', Validators.required],
      ciudad:['', Validators.required],
      provincia:['', Validators.required],
    });
  }
  ngOnInit() {
    
  }

  siguientePagina(){
    console.log(this.carritoForm);
    this.router.navigate(['/stage2', this.carritoForm]);

  }

}
