import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, Validator } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-stage3',
  templateUrl: './stage3.page.html',
  styleUrls: ['./stage3.page.scss'],
})
export class Stage3Page implements OnInit {

  forms = [];
  sub: any;
  pagoForm: FormGroup;

  constructor(private router:Router, private route: ActivatedRoute, public fb:FormBuilder) {
    this.sub = this.route.params.subscribe(params => {
      this.forms = params['forms'];
    });
    this.pagoForm = this.fb.group({metodo:['', Validators.required],
      direccion:['', Validators.required],
      terminos:[false, Validators.required]
    });
    this.forms.push(this.pagoForm);
  }

  ngOnInit() {
  }

  volverInicio(){
    this.router.navigate(['/stage1'])
  }

  final(){
    console.log(this.forms);
    this.router.navigate(['/stage4', this.forms]);
  
  
  }

}
